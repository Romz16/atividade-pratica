# DOCS : Atividade Prática
 
Repositório para postagens de commits das  atividades práticas
 
 
## Questões prática
### 3 - Crie nesse repositório um arquivo que vai se chamar `calculadora.js`, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:
 
~~~javascript
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
~~~
 
### Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:
 
O process.argv é uma matriz que contém os argumentos da linha de comando. O primeiro elemento será 'node', o segundo elemento será o nome do arquivo JavaScript. Os próximos elementos serão quaisquer argumentos adicionais da linha de comando.Portanto, uma process.argv.slice(2) usará os próximos argumentos [0][1][2] ( cortará os próximos 2 dados) ,porém, a soma usará apenas os args[0] e [1].EX: node calculadora.js 10 20 30 40, será utilizado somente o 10 e o 20. Esses são os argumentos que serão usados ​​para construir a string que posteriormente se converterá em inteiro, caso seja possível a conversão, e então teremos o resultado 30 . Para executar basta digitar no terminal : node calculadora.js( números que serão somados).
 
### 4 - Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele. Que tipo de commit esse código deve ter de acordo ao conventional commit. Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Por fim, faça um push desse commit.
 
-**CÓDIGO**:feat: Tratam adições de novas funcionalidades ou de quaisquer outras novas implantações ao código;
 
-**README.MD**: docs: referem-se a inclusão ou alteração somente de arquivos de documentação;
 
 
### 5 - Copie e cole o código abaixo em sua calculadora.js:
 
~~~javascript
const soma = () => {
   console.log(parseInt(args[0]) + parseInt(args[1]));
};
 
const args = process.argv.slice(2);
 
soma();
 
~~~
### Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.
 
Essa mudança representa uma perf: Uma alteração de código que melhora o desempenho.
 
### 6 - João entrou em seu repositório e o deixou da seguinte maneira:
 
~~~javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
~~~
 
### Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction" faça como ele e descubra como executar o seu novo código. Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e divisão funcionem. Por fim, commit sua mudança.
Os erros estão na linha de default, onde esta escrito “arg” ao invés de “args”,o outro erro pode ser corrigido de duas maneiras:
**1°**  Substitua o por “2” o “0” em const args e no “args[0]” do default. 
**2°** Substitua o “args[0]” e o “args[1]” por “args[1]” e “args[2]” respectivamente, tanto na função de soma quanto na de subtração.
 
### 7 - Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer. Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.
 
 
### 8 - Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.
 
 
### 9 - João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, faça a seguinte alteração no código e fez o commit com a mensagem: **"refactor: calculator abstraction"**
 
~~~javascript
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}


~~~
### Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!
 
 
 
### 10 - Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.
Para executar esse programa basta digitar : node calculadora.js 1°número, (sinal de   operação), 2°número, assim o programa será executado. O código pega os argumentos [0],[1] e [2], de forma que o primeiro argumento será definido como x(1°número), o segundo como operator( sinal de operação) e o terceiro como y(segundo número).Após isso, se tem a função evaluate que pega os parâmetros, numero 1, numero 2 e sinal, respectivamente e retorna o resultado da operação definida pelo sinal digitado. Por fim, se tem um if sem condições que chama a função e passa os valores de x,y e operator para a mesma, assim executando os procedimentos matemáticos.
